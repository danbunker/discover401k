require File.dirname(__FILE__) + '/../test_helper'
require 'client_documents_controller'

# Re-raise errors caught by the controller.
class ClientDocumentsController; def rescue_action(e) raise e end; end

class ClientDocumentsControllerTest < Test::Unit::TestCase
  fixtures :client_documents

  def setup
    @controller = ClientDocumentsController.new
    @request    = ActionController::TestRequest.new
    @response   = ActionController::TestResponse.new

    @first_id = client_documents(:first).id
  end

  def test_index
    get :index
    assert_response :success
    assert_template 'list'
  end

  def test_list
    get :list

    assert_response :success
    assert_template 'list'

    assert_not_nil assigns(:client_documents)
  end

  def test_show
    get :show, :id => @first_id

    assert_response :success
    assert_template 'show'

    assert_not_nil assigns(:client_document)
    assert assigns(:client_document).valid?
  end

  def test_new
    get :new

    assert_response :success
    assert_template 'new'

    assert_not_nil assigns(:client_document)
  end

  def test_create
    num_client_documents = ClientDocument.count

    post :create, :client_document => {}

    assert_response :redirect
    assert_redirected_to :action => 'list'

    assert_equal num_client_documents + 1, ClientDocument.count
  end

  def test_edit
    get :edit, :id => @first_id

    assert_response :success
    assert_template 'edit'

    assert_not_nil assigns(:client_document)
    assert assigns(:client_document).valid?
  end

  def test_update
    post :update, :id => @first_id
    assert_response :redirect
    assert_redirected_to :action => 'show', :id => @first_id
  end

  def test_destroy
    assert_nothing_raised {
      ClientDocument.find(@first_id)
    }

    post :destroy, :id => @first_id
    assert_response :redirect
    assert_redirected_to :action => 'list'

    assert_raise(ActiveRecord::RecordNotFound) {
      ClientDocument.find(@first_id)
    }
  end
end
