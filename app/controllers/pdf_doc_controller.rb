class PdfDocController < AdminController
	def delete_pdf
		PdfDoc.delete(params[:pdf_doc_id])
		flash[:tab] = 3
		redirect_to_index(nil)
  end

	def find_document
		search = "%#{params[:search_text]}%"
		@pdf_pages, @pdfs = paginate :pdf_doc, :conditions => ['pdf_name like ?', search], :order_by => 'pdf_name'
		render :update do |page|
			page.replace_html  'pdf_list', :partial => 'admin/pdfs_list'
		end
  end

	def edit_pdf
		logger.info("edit pdf: #{params[:pdf_id]}");
		@pdf_doc = PdfDoc.new
 		@pdf_doc = PdfDoc.find(params[:pdf_id]) if !params[:pdf_id].nil?

		render :update do |page|
			page.replace_html  'pdf_edit', :partial => 'pdf_doc/doc_edit'
		end
  end
	
	def add_pdf
		logger.info("pdf pk: #{params[:pdf_doc][:id].to_s}")
		
		@pdf_doc = PdfDoc.find(params[:pdf_doc][:id]) if params[:pdf_doc][:id] != ""
		@pdf_doc = PdfDoc.new(params[:pdf_doc]) if params[:pdf_doc][:id] == ""

		if !params[:company_id].nil?
			params[:company_id].each do |id|
				company = Company.find(id)
				@pdf_doc.companies << company
			end
		end
		
		@pdf_doc.save
		flash[:tab] = 3
		flash[:errors] = @pdf_doc.errors if @pdf_doc.errors.size > 0
		
		redirect_to_index("PDF Doc: '#{params[:pdf_name]}' successfully added.")
  end
end
