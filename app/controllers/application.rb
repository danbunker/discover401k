# Filters added to this controller apply to all controllers in the application.
# Likewise, all the methods added will be available for all controllers.

class ApplicationController < ActionController::Base
  # Pick a unique cookie name to distinguish our session data from others'
  session :session_key => '_discover401k.com_session_id'

  before_filter :check_peo

  private
  def check_peo
    uri = request.env["REQUEST_URI"]

    logger.warn("URI: #{uri}")

    found_match = uri.scan(/\/peo/)
    if found_match.length >0
      #we already are at the peo section so just continue

      uri = request.host

      found_match = uri.scan(/samplepeo401k|localhost|192.168/)
      if found_match.length >0
        flash[:company_id] = 'samplepeo'
      end

      found_match = uri.scan(/swan401k/)
      if found_match.length >0
        flash[:company_id] = 'swan401k'
      end

      found_match = uri.scan(/rmi401k/)
      if found_match.length >0
        flash[:company_id] = 'rmi401k'
      end

      found_match = uri.scan(/samplecompany401k/)
      if found_match.length >0
        flash[:company_id] = 'samplecompany401k'
      end

      found_match = uri.scan(/oasis401k/)
      if found_match.length >0
        flash[:company_id] = 'oasis401k'
      end

      found_match = uri.scan(/myrpc401k/)
      if found_match.length >0
        flash[:company_id] = 'myrpc401k'
      end

      found_match = uri.scan(/sampleadvisor401k/)
      if found_match.length >0
        flash[:company_id] = 'sampleadvisor401k'
      end

      return true
    end

    found_match = uri.scan(/showpeo=true/)
    if found_match.length >0
      flash[:company_id] = 'samplepeo'
      redirect_to "http://#{request.host}:#{request.port}/peo"
    end

    uri = request.host

    found_match = uri.scan(/samplepeo401k/)
    if found_match.length >0
      flash[:company_id] = 'samplepeo'
      redirect_to "http://#{request.host}:#{request.port}/peo"
    end
    
    found_match = uri.scan(/samplecompany401k/)
    if found_match.length >0
      flash[:company_id] = 'samplecompany401k'
      redirect_to "http://#{request.host}:#{request.port}/peo"
    end

    found_match = uri.scan(/swan401k/)
    if found_match.length >0
      flash[:company_id] = 'swan401k'
      redirect_to "http://#{request.host}:#{request.port}/peo"
    end

    found_match = uri.scan(/rmi401k/)
    if found_match.length >0
      flash[:company_id] = 'rmi401k'
      redirect_to "http://#{request.host}:#{request.port}/peo"
    end

    found_match = uri.scan(/oasis401k/)
    if found_match.length >0
      flash[:company_id] = 'oasis401k'
      redirect_to "http://#{request.host}:#{request.port}/peo"
    end

    found_match = uri.scan(/myrpc401k/)
    if found_match.length >0
      flash[:company_id] = 'myrpc401k'
      redirect_to "http://#{request.host}:#{request.port}/peo"
    end

    found_match = uri.scan(/sampleadvisor401k/)
    if found_match.length >0
      flash[:company_id] = 'sampleadvisor401k'
      redirect_to "http://#{request.host}:#{request.port}/peo"
    end

    return true
  end
end
