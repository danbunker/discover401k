class CompanyClientsController < ApplicationController
	htpasswd :user=>"401kadmin", :pass=>"spencer06"

  def index
    list
    render :action => 'list'
  end

  # GETs should be safe (see http://www.w3.org/2001/tag/doc/whenToUseGet.html)
  verify :method => :post, :only => [ :destroy, :create, :update ],
         :redirect_to => { :action => :list }

  def list
    @company_client_pages, @company_clients = paginate :company_clients, :per_page => 10
  end

  def show
    @company_client = CompanyClient.find(params[:id])
  end

  def new
    @companies = Company.find(:all, {:order => 'name ASC'})
    @company_client = CompanyClient.new
    @company_client.company = Company.new
  end

  def create
    @company_client = CompanyClient.new(params[:company_client])
    @company_client.company = Company.find_by_id(params[:company_id])
    if @company_client.save
      flash[:notice] = 'CompanyClient was successfully created.'
      redirect_to :action => 'list'
    else
      render :action => 'new'
    end
  end

  def edit
    @companies = Company.find(:all, {:order => 'name ASC'})
    @company_client = CompanyClient.find(params[:id])
  end

  def update
    @company_client = CompanyClient.find(params[:id])
    @company_client.company = Company.find_by_id(params[:company_id])
    if @company_client.update_attributes(params[:company_client])
      flash[:notice] = 'CompanyClient was successfully updated.'
      redirect_to :action => 'list'
    else
      render :action => 'edit'
    end
  end

  def destroy
    CompanyClient.find(params[:id]).destroy
    redirect_to :action => 'list'
  end
end
