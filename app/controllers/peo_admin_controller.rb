class PeoAdminController < ApplicationController
	def redirect_to_index(msg = nil)
		flash[:notice] = msg if msg
		logger.debug("Validation Message #{flash[:notice]}")		
		redirect_to :action => :index
	end

  def index
	  @company = Company.find_by_identifier(flash[:company_id])
	  logger.debug("Company #{@company}")
  end
  
  def admin
    if flash[:company].nil?
		  @company = Company.find_by_peo_username(params[:username])
  		if @company.nil? || @company.peo_password != params[:password]
  			logger.error("Couldn't find PEO User #{params[:username]}")
  			redirect_to_index("Invalid Username or Password.  Contact Discover401k if this problem persists.")
  	  end
		else
		  @company = flash[:company]
	  end

  end
  
  def save_peo
		@company = Company.find(params[:company][:id])
    @company.attributes = params[:company]
    @company.save
		flash[:company] = @company
		flash[:notice] = "PEO Information Updated"
		redirect_to :action => :admin
  end

  def add_form
		@company = Company.find(params[:company_id])
		@pdf_doc = PdfDoc.new
    @pdf_doc.attributes = params[:pdf_doc]
    @pdf_doc.companies << @company
    @pdf_doc.save

		flash[:company] = @company
		flash[:notice] = "New PEO Form Added"
		redirect_to :action => :admin
  end

  def delete_doc
    @company = Company.find(params[:company_id])
		PdfDoc.delete(params[:form_id])

		flash[:company] = @company
		flash[:notice] = "PEO Form Deleted"
		redirect_to :action => :admin
  end

  def add_link
		@company = Company.find(params[:company_id])
		@link = Link.new
    @link.attributes = params[:link]
    @link.companies << @company
    @link.save

		flash[:company] = @company
		flash[:notice] = "New PEO Link Added"
		redirect_to :action => :admin
  end

  def delete_link
    @company = Company.find(params[:company_id])
		Link.delete(params[:link_id])

		flash[:company] = @company
		flash[:notice] = "PEO Link Deleted"
		redirect_to :action => :admin
  end
end
