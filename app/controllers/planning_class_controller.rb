class PlanningClassController < AdminController
	def delete_class
		PlanningClass.delete(params[:planning_class_id])
		flash[:tab] = 2
		redirect_to_index(nil)
  end

	def edit_class
		logger.info("edit class: #{params[:class_id]}");
		@planning_class = PlanningClass.new
 		@planning_class = PlanningClass.find(params[:class_id]) if !params[:class_id].nil?

		render :update do |page|
			page.replace_html  'class_edit', :partial => 'planning_class/class_edit'
		end
  end
	
	def find_class
		search = "%#{params[:search_text]}%"
		@planning_class_pages, @planning_classes = paginate :planning_classs, :conditions => ['class_name like ?', search], :order_by => 'class_name'
		render :update do |page|
			page.replace_html  'class_list', :partial => 'admin/classes_list'
		end
  end

	def add_class
		logger.info("class pk: #{params[:pcid].to_s}")

		@planning_class = PlanningClass.find(params[:pcid]) if params[:pcid] != ""
		@planning_class = PlanningClass.new if params[:pcid] == ""
		@planning_class.class_name = params[:class_name]
		@planning_class.class_url = params[:class_url]
		@planning_class.spanish_class_url = params[:spanish_class_url]
		@planning_class.section = params[:section]
		@planning_class.header = params[:header]
		@planning_class.display_sequence = params[:display_sequence]
		@planning_class.default_class_flag = false
		@planning_class.default_class_flag = true if !params[:default_class_flag].nil?
		@planning_class.admin_name = params[:admin_name]
		@planning_class.eddy_award = false
		@planning_class.eddy_award = true if !params[:eddy_award].nil?
		@planning_class.display_in_bold = false
		@planning_class.display_in_bold = true if !params[:display_in_bold].nil?
		@planning_class.free_trial_flag = false
		@planning_class.free_trial_flag = true if !params[:free_trial_flag].nil?
		
		@planning_class.save
		flash[:tab] = 2
		flash[:errors] = @planning_class.errors if @planning_class.errors.size > 0

		setup_class		
  end
end
