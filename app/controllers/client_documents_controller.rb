class ClientDocumentsController < ApplicationController
	htpasswd :user=>"401kadmin", :pass=>"spencer06"

  def index
    list
    render :action => 'list'
  end

  # GETs should be safe (see http://www.w3.org/2001/tag/doc/whenToUseGet.html)
  verify :method => :post, :only => [ :destroy, :create, :update ],
         :redirect_to => { :action => :list }

  def list
    @client_document_pages, @client_documents = paginate :client_documents, :per_page => 10
  end

  def show
    @client_document = ClientDocument.find(params[:id])
  end

  def new
    @company_client = CompanyClient.find(params[:id])
    @client_document = ClientDocument.new
  end

  def create
    @company_client = CompanyClient.find(params[:client_id])
    @client_document = ClientDocument.new(params[:client_document])
    @client_document.company_client = @company_client
    if @client_document.save
      flash[:notice] = 'ClientDocument was successfully created.'
      redirect_to :controller => 'company_clients', :action => 'show', :id => @company_client
    else
      render :action => 'new'
    end
  end

  def edit
    @client_document = ClientDocument.find(params[:id])
  end

  def update
    @client_document = ClientDocument.find(params[:id])
    if @client_document.update_attributes(params[:client_document])
      flash[:notice] = 'ClientDocument was successfully updated.'
      redirect_to :action => 'show', :id => @client_document
    else
      render :action => 'edit'
    end
  end

  def destroy
    cd = ClientDocument.find(params[:id])
    ClientDocument.find(params[:id]).destroy
    redirect_to :controller => 'company_clients', :action => 'show', :id => cd.company_client
  end
end
