class PeoController < ApplicationController
	def redirect_to_index(msg = nil)
		flash[:notice] = msg if msg
		logger.debug("Validation Message #{flash[:notice]}")		
		redirect_to :action => :index
	end

  def get_company_logo
		logger.debug("Getting image by ID #{params[:id]}")		
    c = Company.find_by_id(params[:id])
		logger.debug("Company logo type #{c.logo_img_type}")		
    return send_data(c.logo, :type => c.logo_img_type, :disposition => "inline")
  end

	def disclosure
	  @company = Company.find(params[:company_id])
	  logger.debug("Company #{@company}")
  end
  
	def privacy
	  @company = Company.find(params[:company_id])
	  logger.debug("Company #{@company}")
  end

	def terms
	  @company = Company.find(params[:company_id])
  end

  def index
	  @company = Company.find_by_identifier(flash[:company_id])
	  logger.debug("Company #{@company}")
  end

  def login
  	begin
  		@company = Company.find_by_identifier(params[:company_id])
			
			if @company.nil?
				logger.error("No company identifier #{params[:company_id]}")
				redirect_to_index("Invalid Company ID: '#{params[:company_id]}'.  Please enter a valid Company ID.")
			elsif params[:email] == "" || !(params[:email] =~ /\A[\w\._%-]+@[\w\.-]+\.[a-zA-Z]{2,4}\z/)
				logger.error("No email address provided")
				redirect_to_index("Invalid Email: '#{params[:email]}'.  Please enter a valid Email Address.")
  		elsif @company.company_clients.size > 0 && (params[:client_id] == "" || @company.contains_client?(params[:client_id]) == false)
  				logger.error("Invalid Client ID #{params[:client_id]}")
  				redirect_to_index("Invalid Client ID: '#{params[:client_id]}'.  Please enter a valid Client ID.")
			else
				sub = Subscriber.find_by_email_and_company_id(params[:email], @company.id)
				
				logger.info("Found subscriber: #{sub}")
				if sub.nil?
					sub = Subscriber.new
					sub.email = params[:email]
					@company.subscribers << sub
				end
				
				sub.contact_me_flag = false
				sub.contact_me_flag = true if !params[:contact_me].nil?
				sub.last_login_date = Time.now
    		sub.save

				@classes = PlanningClass.find(:all,
					:joins => "as pc left outer join companies_planning_classes as cpc on pc.id = cpc.planning_class_id", 
					:conditions => "((pc.default_class_flag = true) or (cpc.company_id = '#{@company.id}')) " , 
					:order => "pc.display_sequence ASC",
					:select => "distinct pc.* " )
				
				@classes_to_hide = PlanningClass.find(:all,
					:joins => "as pc inner join companies_planning_classes as cpc on pc.id = cpc.planning_class_id", 
					:conditions => "(cpc.company_id = '#{@company.id}' and cpc.hidden_flag = true) " , 
					:order => "pc.display_sequence ASC",
					:select => "distinct pc.* " )

				@classes = @classes - @classes_to_hide
					
				@client_documents = @company.client_documents_for_client(params[:client_id])
			end
  	rescue ActiveRecord::RecordNotFound
  		logger.error("Attempt to login with an invalid company id #{params[:company_id]}")
			redirect_to_index("Invalid Company ID: '#{params[:company_id]}'.  Please enter a valid Company ID.")
  	end
  end

	def get_pdf
		pdf = PdfDoc.find(params[:id])
		send_data(pdf.pdf, :filename => pdf.file_name, :type => pdf.doc_type, :disposition => 'inline')
	end

	def get_client_doc
		cd = ClientDocument.find(params[:id])
		send_data(cd.doc_content, :filename => cd.doc_name, :type => cd.doc_type, :disposition => 'inline')
	end
end
