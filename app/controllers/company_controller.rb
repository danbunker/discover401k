class CompanyController < AdminController
	def add_company
		logger.info("Company pk: #{params[:cid].to_s}")

		@company = Company.find(params[:company][:id]) if params[:company][:id] != ""
		@company = Company.new(params[:company]) if params[:company][:id] == ""
		
		@company.attributes = params[:company]

    # company = Company.find(params[:cid]) if params[:cid] != ""
    # company = Company.new if params[:cid] == ""
    # company.identifier = params[:company_id]
    # company.name = params[:company_name]
    # company.employee_flag = false
    # company.employee_flag = true if !params[:employee_flag].nil?
    # company.free_trial_flag = false
    # company.free_trial_flag = true if !params[:free_trial_flag].nil?
    # company.disclosure = params[:disclosure]
    # company.override_on_demand_url = params[:override_on_demand_url]
		
		cpc_array = []
		add_array = []
		
		if !params[:company_edit].nil? && !params[:company_edit][:class_ids].nil?
			params[:company_edit][:class_ids].each do |class_id|
				cpc = CompaniesPlanningClass.find_by_company_id_and_planning_class_id(@company.id, class_id)
				cpc_array << cpc if !cpc.nil?
				add_array << PlanningClass.find(class_id) if cpc.nil?
			end
		end
		
		pdf_array = []
		
		if !params[:company_edit].nil? && !params[:company_edit][:pdf_ids].nil?
			params[:company_edit][:pdf_ids].each do |pdf_id|
				pd = PdfDoc.find(pdf_id)
				pdf_array << pd 
			end
		end

		link_array = []
		
		if !params[:company_edit].nil? && !params[:company_edit][:link_ids].nil?
			params[:company_edit][:link_ids].each do |link_id|
				l = Link.find(link_id)
				link_array << l 
			end
		end

		@company.save

		CompaniesPlanningClass.delete_all ["company_id = ? AND planning_class_id in (?)", @company.id, (@company.companies_planning_classes - cpc_array).uniq.collect { |pc| pc.planning_class_id }]
		@company.companies_planning_classes << cpc_array
		@company.planning_classes << add_array
		
		@company.pdf_docs.clear
		@company.pdf_docs << pdf_array.uniq
					
		@company.links.clear
		@company.links << link_array.uniq

		@company.save
		
		flash[:errors] = @company.errors if @company.errors.size > 0
		
		setup_company
		redirect_to_index("Company: '#{@company.name}' successfully updated or added.")
		
    # if params[:id] != ""
    #   render :update do |page|
    #     page.replace_html 'company_list', :partial => 'admin/companies_list'
    #     page.hide 'company_edit'
    #     page.visual_effect :highlight, "comp_row_#{@company.id}", :startcolor => "'#ffff00'", :endcolor => "'#a7b5bd'", :duration => 2.5 
    #   end
    # else
    #   render :update do |page|
    #     page.replace_html 'msgs', "Company Added Successfully"
    #     page.hide 'company_edit'
    #     page.visual_effect :highlight, "msgs", :startcolor => "'#ffff00'", :endcolor => "'#a7b5bd'", :duration => 2.5 
    #   end
    # end
  end

	def edit_company
		logger.info("edit company: #{params[:company_id]}");
		@company = Company.new
 		@company = Company.find(params[:company_id]) if !params[:company_id].nil?
		
		@all_classes = PlanningClass.find(:all, :order => 'class_name ASC' )
		@all_docs = PdfDoc.find(:all, :order => 'pdf_name ASC' )
		@all_links = Link.find(:all, :order => 'link_name ASC' )
		render :update do |page|
			page.replace_html  'company_edit', :partial => 'company/company_edit'
		end
  end

	def delete_company
		Company.delete(params[:company_id])
		redirect_to_index(nil)
  end

	def show_subscribers
		if params[:company_for_subs_id] != ""
			flash[:company_for_subs_id] = params[:company_for_subs_id]
		end
		
		redirect_to_index(nil)
  end

	def delete_subscriber
		Subscriber.delete(params[:subscriber_id])
		show_subscribers
  end

	def show_classes
		if params[:company_for_class_id] != ""
			flash[:company_for_class_id] = params[:company_for_class_id]
		end
		
		redirect_to_index(nil)
  end

	def find_company
		session[:company_identifier] = params[:company_identifier] + "%"
		
		redirect_to_index(nil)
  end

	def find_company_name
		session[:company_name] = params[:company_name] + "%"
		
		redirect_to_index(nil)
  end

	def remove_class
#		pc = PlanningClass.find(params[:class_id])
#		comp = Company.find(params[:company_for_class_id])
		CompaniesPlanningClass.delete([params[:company_for_class_id], params[:class_id]])
#		comp.companies_planning_classes.delete(pcp)
#		comp.planning_classes.delete(pc)
#		comp.save
		show_classes
  end

	def add_class
		company = Company.find(params[:company_for_class_id])

		if !params[:class_id].nil?
			params[:class_id].each do |id|
				pc = PlanningClass.find(id)
				company.planning_classes << pc
			end
		end
		
		company.save
		show_classes
  end

	def toggle_visibility
		cpc = CompaniesPlanningClass.find [params[:company_for_class_id], params[:class_id]]
		cpc.hidden_flag = !cpc.hidden_flag
		cpc.save
		show_classes
  end
end
