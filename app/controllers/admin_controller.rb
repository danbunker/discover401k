class AdminController < ApplicationController
	htpasswd :user=>"401kadmin", :pass=>"spencer06"
	
	def index
		setup_company
		setup_class
		setup_pdf
		setup_link
		
		logger.info("request.xhr?: #{request.xhr?}")
		logger.info("flash: #{flash}")

		if request.xhr? && params[:tab] == "1"
			render :update do |page|
				page.replace_html  'company_list', :partial => 'companies_list'
			end
		elsif request.xhr? && params[:tab] == "2"
			render :update do |page|
				page.replace_html  'class_list', :partial => 'classes_list'
			end
		elsif request.xhr? && params[:tab] == "3"
			render :update do |page|
				page.replace_html  'pdf_list', :partial => 'pdfs_list'
			end
		elsif request.xhr? && params[:tab] == "4"
			render :update do |page|
				page.replace_html  'link_list', :partial => 'link_list'
			end
		end
	end
	
	def redirect_to_index(msg = nil)
		logger.debug("Errors: #{flash[:errors]}")
		@errors = flash[:errors] if flash[:errors] 
		flash[:notice] = msg if msg && !flash[:errors]
		redirect_to :controller => :admin, :action => :index
	end

	private
	
	def setup_company
		@company_pages, @companies = paginate :company, :order_by => 'name, identifier', :per_page => 50
		
		logger.info("Session INFO for company identifier #{session[:company_identifier]}")

		if !session[:company_identifier].nil? && session[:company_identifier] != ""
			@company_pages, @companies = paginate :company, :conditions => ['identifier like ?', session[:company_identifier]], :order_by => 'name, identifier'
			session[:company_identifier] = session[:company_identifier]
		end

		if !session[:company_name].nil? && session[:company_name] != ""
			@company_pages, @companies = paginate :company, :conditions => ['name like ?', session[:company_name]], :order_by => 'name, identifier'
			session[:company_name] = session[:company_name]
		end

		logger.info("company: #{params[:company_id]}");
		@company = Company.new if flash[:company_id].nil?
 		@company = Company.find(flash[:company_id]) if !flash[:company_id].nil?

		if flash[:company_for_subs_id]
			company = Company.find(flash[:company_for_subs_id])
			@subscribers = company.subscribers
			
			logger.info("showing subscribers for company: #{flash[:company_for_subs_id]} : #{@subscribers.size}")
			flash[:notice] = "Company doesn't currently have any subscribers" if @subscribers.size <= 0
		end

		if flash[:company_for_class_id]
			company = Company.find(flash[:company_for_class_id])
			@classes = company.companies_planning_classes
			
			logger.info("showing classes for company: #{flash[:company_for_class_id]} : #{@classes.size}")
			flash[:notice] = "Company doesn't currently have any customized classes" if @classes.size <= 0
		end

		if flash[:company_for_pdf_id]
			company = Company.find(flash[:company_for_pdf_id])
			@cpdfs = company.pdf_docs
			
			logger.info("showing pdfs for company: #{flash[:company_for_pdf_id]} : #{@cpdfs.size}")
			flash[:notice] = "Company doesn't currently have any customized PDF's" if @cpdfs.size <= 0
		end
	end
	
	def setup_class
		@planning_class_pages, @planning_classes = paginate :planning_classs, :order_by => 'class_name', :per_page => 50

		logger.info("class: #{params[:class_id]}");
		@planning_class = PlanningClass.new if flash[:class_id].nil?
 		@planning_class = PlanningClass.find(flash[:class_id]) if !flash[:class_id].nil?
	end

	def setup_pdf
		@pdf_pages, @pdfs = paginate :pdf_doc, :order_by => 'pdf_name', :per_page => 50

		logger.info("pdf: #{params[:pdf_id]}");
		@pdf_doc = PdfDoc.new if flash[:pdf_id].nil?
 		@pdf_doc = PdfDoc.find(flash[:pdf_id]) if !flash[:pdf_id].nil?
	end

	def setup_link
		@link_pages, @links = paginate :link, :order_by => 'link_name', :per_page => 50

		logger.info("link: #{params[:link_id]}");
		@link = Link.new if flash[:link_id].nil?
 		@link = Link.find(flash[:link_id]) if !flash[:link_id].nil?
	end
end
