class LinkController < AdminController
		def delete_link
		Link.delete(params[:link_id])
		flash[:tab] = 4
		redirect_to_index(nil)
  end

	def find_link
		search = "%#{params[:search_text]}%"
		@link_pages, @links = paginate :link, :conditions => ['link_name like ?', search], :order_by => 'link_name'
		render :update do |page|
			page.replace_html  'link_list', :partial => 'admin/link_list'
		end
  end

	def edit_link
		logger.info("edit link: #{params[:link_id]}");
		@link = Link.new
 		@link = Link.find(params[:link_id]) if !params[:link_id].nil?

		render :update do |page|
			page.replace_html  'link_edit', :partial => 'link/link_edit'
		end
  end
	
	def add_link
		logger.info("link pk: #{params[:id].to_s}")
		
		@link = Link.find(params[:id]) if params[:id] != ""
		@link = Link.new if params[:id] == ""
		@link.link_name = params[:link_name]
		@link.url = params[:url]

		@link.save
		flash[:tab] = 4
		flash[:errors] = @link.errors if @link.errors.size > 0
		
		setup_link
  end
end
