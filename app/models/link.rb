# == Schema Information
# Schema version: 26
#
# Table name: links
#
#  id        :integer(11)   not null, primary key
#  link_name :string(255)   
#  url       :string(255)   
#

class Link < ActiveRecord::Base
	has_and_belongs_to_many :companies

	validates_presence_of :link_name
	validates_presence_of :url
end
