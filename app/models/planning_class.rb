# == Schema Information
# Schema version: 26
#
# Table name: planning_classes
#
#  id                 :integer(11)   not null, primary key
#  class_name         :string(255)   
#  class_url          :string(255)   
#  section            :string(255)   
#  admin_name         :string(255)   
#  display_sequence   :integer(11)   
#  default_class_flag :boolean(1)    
#  header             :string(255)   
#  eddy_award         :boolean(1)    
#  display_in_bold    :boolean(1)    
#  spanish_class_url  :string(255)   
#  free_trial_flag    :boolean(1)    
#

class PlanningClass < ActiveRecord::Base
	has_many :companies_planning_classes
  has_many :companies, :through => :companies_planning_classes 

	validates_presence_of :class_name
	validates_presence_of :class_url
	validates_presence_of :section
end
