# == Schema Information
# Schema version: 26
#
# Table name: subscribers
#
#  id              :integer(11)   not null, primary key
#  company_id      :integer(11)   
#  email           :string(255)   
#  contact_me_flag :boolean(1)    
#  last_login_date :datetime      
#

class Subscriber < ActiveRecord::Base
	belongs_to :company
	
	validates_presence_of :email	

  def login_date_formatted
    if last_login_date.nil?
      return ""
    else
     last_login_date.strftime '%m/%d/%Y %I:%M %p'
   end
  end
end
