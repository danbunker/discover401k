class Mailer < ActionMailer::Base
	def contact(params, sent_at = Time.now)
		 @subject = 'Discover 401k Website Message'
		 @recipients = 'mcirque@gmail.com'
		 @from = 'admin@discover401k.com'
		 @sent_on = sent_at
	 	 @body["company"] = params[:company]
		 @body["email"] = params[:email]
		 @body["name"] = params[:name]
		 @body["phone"] = params[:phone]
		 @body["message"] = params[:message]
		 @headers = {}
	end
end
