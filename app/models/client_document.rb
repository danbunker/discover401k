class ClientDocument < ActiveRecord::Base
	belongs_to :company_client

	def uploaded_file=(file_field)
		self.doc_content = file_field.read if file_field != ""
		self.doc_type = file_field.content_type if file_field != ""
	end
end
