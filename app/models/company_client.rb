class CompanyClient < ActiveRecord::Base
	has_many :client_documents
	belongs_to :company
end
