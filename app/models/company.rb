# == Schema Information
# Schema version: 26
#
# Table name: companies
#
#  id                     :integer(11)   not null, primary key
#  identifier             :string(255)   
#  name                   :string(255)   
#  employee_flag          :boolean(1)    
#  disclosure             :text          
#  override_on_demand_url :string(255)   
#  free_trial_flag        :boolean(1)    
#  logo                   :binary        
#  logo_img_type          :string(255)   
#  logo_filename          :string(255)   
#  company_url            :string(255)   
#  peo_username           :string(255)   
#  peo_password           :string(255)   
#

class Company < ActiveRecord::Base
	has_many :subscribers
  has_many :companies_planning_classes
  has_many :planning_classes, :through => :companies_planning_classes 
	has_many :company_clients
  has_and_belongs_to_many :pdf_docs
  has_and_belongs_to_many :links
	
	validates_presence_of :identifier
	validates_uniqueness_of :identifier

	validates_presence_of :name
	
  def to_s
  	return name
  end
  	
  def contains_client?(client_id)
    has_client = false
    
    company_clients.each do |client| 
      if client.client_id == client_id
        has_client = true
        break
      end
    end
    
	  logger.debug("has_client #{has_client}")
  	return has_client
  end

  def client_documents_for_client(client_id)
    cds = Array.new
    
    company_clients.each do |client| 
      if client.client_id == client_id
        cds = client.client_documents
        break
      end
    end
    
  	return cds
  end

	def uploaded_file=(file_field)
	  logger.debug("Saving Company Logo")
		self.logo = file_field.read if file_field != ""
		self.logo_img_type = file_field.content_type if file_field != ""
		self.logo_filename = sanitize_filename(file_field.original_filename) if file_field != ""
	end

  private
  def sanitize_filename(file_name)
    # get only the filename, not the whole path (from IE)
    just_filename = File.basename(file_name) 
    # replace all none alphanumeric, underscore or perioids with underscore
    just_filename.gsub(/[^\w\.\_]/,'_') 
  end
end
