# == Schema Information
# Schema version: 26
#
# Table name: pdf_docs
#
#  id        :integer(11)   not null, primary key
#  pdf_name  :string(255)   
#  section   :string(255)   
#  pdf       :binary        
#  doc_type  :string(255)   
#  file_name :string(255)   
#

class PdfDoc < ActiveRecord::Base
	has_and_belongs_to_many :companies

	validates_presence_of :pdf_name
	validates_presence_of :pdf
	
	def uploaded_file=(file_field)
		self.pdf = file_field.read if file_field != ""
		self.doc_type = file_field.content_type if file_field != ""
		self.file_name = sanitize_filename(file_field.original_filename) if file_field != ""
	end

  private
  def sanitize_filename(file_name)
    # get only the filename, not the whole path (from IE)
    just_filename = File.basename(file_name) 
    # replace all none alphanumeric, underscore or perioids with underscore
    just_filename.gsub(/[^\w\.\_]/,'_') 
  end
end
