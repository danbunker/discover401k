# == Schema Information
# Schema version: 26
#
# Table name: companies_planning_classes
#
#  company_id        :integer(11)   
#  planning_class_id :integer(11)   
#  hidden_flag       :boolean(1)    
#
require 'composite_primary_keys' 

class CompaniesPlanningClass < ActiveRecord::Base
	set_primary_keys :company_id, :planning_class_id
	belongs_to :company
	belongs_to :planning_class
end
