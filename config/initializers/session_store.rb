# Be sure to restart your server when you modify this file.

# Your secret key for verifying cookie session data integrity.
# If you change this key, all old sessions will become invalid!
# Make sure the secret is at least 30 characters and all random, 
# no regular words or you'll be exposed to dictionary attacks.
ActionController::Base.session = {
  :key         => '_discover_session',
  :secret      => 'ee0e5147474cbdc2a2efce7d7fecf3ab78824376dc2139234b98a44f444ecbb30428edadaaf307630ceee5f3b27f7415ed6739c0502c0db99a403567035b9be4'
}

# Use the database for sessions instead of the cookie-based default,
# which shouldn't be used to store highly confidential information
# (create the session table with "rake db:sessions:create")
# ActionController::Base.session_store = :active_record_store
