set :deploy_to, "/home/dan/stage/#{application}"
set :mongrel_port, 9000
set :mongrel_user, 'dan'
set :mongrel_group, 'dan'
#set :mongrel_prefix, '/ServiceMissionary'

role :app, "dan@home.mountaincirque.net"
role :web, "dan@home.mountaincirque.net"
role :db,  "dan@home.mountaincirque.net", :primary => true
