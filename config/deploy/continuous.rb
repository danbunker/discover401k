set :deploy_to, "/home/dan/continuous/#{application}"
set :mongrel_port, 5020
set :mongrel_user, 'dan'
set :mongrel_group, 'dan'
set :mongrel_prefix, '/discover401k'
set :rake, "/var/lib/gems/1.8/bin/rake"

role :app, "dan@home.mountaincirque.net"
role :web, "dan@home.mountaincirque.net"
role :db,  "dan@home.mountaincirque.net", :primary => true
