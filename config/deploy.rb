require 'lib/mongrel_cluster_recipes'

set :application, 'Discover401k'

set :repository,  'https://svn.mountaincirque.net/discover401k/trunk/'
set(:scm_username) { Capistrano::CLI.ui.ask("Subversion username: ") } unless variables[:scm_username] 
set(:scm_password) { Capistrano::CLI.password_prompt("Subversion password: ") } unless variables[:scm_password] 

set :deploy_via, :remote_cache
set :use_sudo, false


#set :stages, %w(continuous controlled qa ua stage production)
set :stages, %w(continuous)
set :default_stage, 'continuous'
require 'capistrano/ext/multistage'

# If you aren't deploying to /u/apps/#{application} on the target
# servers (which is the default), you can specify the actual location
# via the :deploy_to variable:
#set :deploy_to, "/home/jared/#{application}"

# If you aren't using Subversion to manage your source code, specify
# your SCM below:
# set :scm, :subversion

#role :app, "your app-server here"
#role :web, "your web-server here"
#role :db,  "your db-server here", :primary => true

#def relative_path(from_str, to_str)
#  require 'pathname'
#  Pathname.new(to_str).relative_path_from(Pathname.new(from_str)).to_s
#end


namespace :deploy do

  desc 'Setup database.yml by linking to pre-configured shared/config/database.yml'
  task :setup_database_yml, :roles => [:app, :db] do
	  run "ln -nfs #{shared_path}/config/database.yml #{release_path}/config/database.yml"
  end
#  
#  desc 'Run the asset:packager:build_all task to generate the production mode assets'
#  task :build_assets, :roles => [:app] do
#	  run "cd #{release_path} && rake RAILS_ENV=production asset:packager:build_all"
#  end
#  
#  desc "Modified versino of default symlink task which uses a relative link"
#  task :symlink, :except => { :no_release => true } do
#  	previous_release_relative = relative_path(deploy_to, previous_release)
#	latest_release_relative = relative_path(deploy_to, latest_release)
#    on_rollback { run "rm -f #{current_path}; ln -s #{previous_release_relative} #{current_path}; true" }
#    run "rm -f #{current_path} && ln -s #{latest_release_relative} #{current_path}"
#  end
#  

	after "deploy:update_code", 'deploy:setup_database_yml'
end