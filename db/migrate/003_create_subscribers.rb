class CreateSubscribers < ActiveRecord::Migration
  def self.up
    create_table :subscribers do |t|
    	t.column :company_id, :integer
      t.column :email, :string
    end
  end

  def self.down
    drop_table :subscribers
  end
end
