class AddSubscriberLoginDate < ActiveRecord::Migration
  def self.up
  	add_column :subscribers, :last_login_date, :timestamp
  end

  def self.down
  end
end
