class AddHeaderColumn < ActiveRecord::Migration
  def self.up
  	add_column :planning_classes, :header, :string
  end

  def self.down
  end
end
