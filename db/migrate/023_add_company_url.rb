class AddCompanyUrl < ActiveRecord::Migration
  def self.up
  	add_column :companies, :company_url, :string
  end

  def self.down
  end
end
