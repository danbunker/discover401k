class AddSpanishColumn < ActiveRecord::Migration
  def self.up
  	add_column :planning_classes, :spanish_class_url, :string
  end

  def self.down
  end
end
