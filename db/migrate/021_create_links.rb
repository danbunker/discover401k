class CreateLinks < ActiveRecord::Migration
  def self.up
    create_table :links do |t|
      t.column :link_name, :string
      t.column :url, :string
    end

    create_table :companies_links, :id => false do |t|
    	t.column :company_id, :integer
      t.column :link_id, :integer
    end
  end

  def self.down
    drop_table :links
    drop_table :companies_links
  end
end
