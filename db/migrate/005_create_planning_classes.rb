class CreatePlanningClasses < ActiveRecord::Migration
  def self.up
    create_table :planning_classes do |t|
    	t.column :class_name, :string
    	t.column :class_url, :string
    	t.column :section, :string
    end

    create_table :companies_planning_classes, :id => false do |t|
    	t.column :company_id, :integer
      t.column :panning_class_id, :integer
    end
  end

  def self.down
  	drop_table :companies_planning_classes
    drop_table :planning_classes
  end
end
