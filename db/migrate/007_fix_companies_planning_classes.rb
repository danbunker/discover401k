class FixCompaniesPlanningClasses < ActiveRecord::Migration
  def self.up
  	rename_column(:companies_planning_classes, :panning_class_id, :planning_class_id)
  end

  def self.down
  end
end
