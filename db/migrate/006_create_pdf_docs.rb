class CreatePdfDocs < ActiveRecord::Migration
  def self.up
    create_table :pdf_docs do |t|
    	t.column :pdf_name, :string
    	t.column :section, :string
    end

    create_table :companies_pdf_docs, :id => false do |t|
    	t.column :company_id, :integer
      t.column :pdf_doc_id, :integer
    end
  end

  def self.down
    drop_table :companies_pdf_docs
    drop_table :pdf_docs
  end
end
