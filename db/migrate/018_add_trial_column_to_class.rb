class AddTrialColumnToClass < ActiveRecord::Migration
  def self.up
  	add_column :planning_classes, :free_trial_flag, :boolean
  end

  def self.down
  end
end
