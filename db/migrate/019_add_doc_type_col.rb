class AddDocTypeCol < ActiveRecord::Migration
  def self.up
  	add_column :pdf_docs, :doc_type, :string
  end

  def self.down
  end
end
