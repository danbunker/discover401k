class AddMoreClassColumns < ActiveRecord::Migration
  def self.up
  	add_column :planning_classes, :eddy_award, :boolean
  	add_column :planning_classes, :display_in_bold, :boolean
  end

  def self.down
  end
end
