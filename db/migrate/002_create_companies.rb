class CreateCompanies < ActiveRecord::Migration
  def self.up
    drop_table :blogs

		create_table :companies do |t|
    	t.column :identifier, :integer
      t.column :name, :string    	
    end
  end

  def self.down
    drop_table :companies
  end
end
