class FixClientDocumentsTwo < ActiveRecord::Migration
  def self.up
  	rename_column(:client_documents, :company_client_id, :company_id)
  end

  def self.down
  end
end
