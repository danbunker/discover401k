class AddPeoCredentials < ActiveRecord::Migration
  def self.up
  	add_column :companies, :peo_username, :string
  	add_column :companies, :peo_password, :string
  end

  def self.down
  end
end
