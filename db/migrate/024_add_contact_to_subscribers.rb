class AddContactToSubscribers < ActiveRecord::Migration
  def self.up
  	add_column :subscribers, :contact_me_flag, :boolean
  end

  def self.down
  end
end
