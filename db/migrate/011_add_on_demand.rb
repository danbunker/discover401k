class AddOnDemand < ActiveRecord::Migration
  def self.up
  	add_column :companies, :override_on_demand_url, :string

  	add_column :planning_classes, :display_sequence, :integer
  	add_column :companies, :default_class_flag, :boolean
  end

  def self.down
  end
end
