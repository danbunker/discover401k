class AddFileNameCol < ActiveRecord::Migration
  def self.up
  	add_column :pdf_docs, :file_name, :string
  end

  def self.down
  end
end
