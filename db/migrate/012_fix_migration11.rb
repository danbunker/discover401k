class FixMigration11 < ActiveRecord::Migration
  def self.up
  	add_column :planning_classes, :default_class_flag, :boolean
		remove_column :companies, :default_class_flag
  end

  def self.down
  end
end
