class CreateClientDocuments < ActiveRecord::Migration
  def self.up
    create_table :client_documents do |t|
    	t.column :company_client_id, :integer
    	t.column :doc_name, :string
    	t.column :doc_content, :binary, :limit => 5.megabytes
    end
  end

  def self.down
    drop_table :client_documents
  end
end
