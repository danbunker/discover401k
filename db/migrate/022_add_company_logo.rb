class AddCompanyLogo < ActiveRecord::Migration
  def self.up
  	add_column :companies, :logo, :binary, :limit => 2.megabytes
  	add_column :companies, :logo_img_type, :string
  	add_column :companies, :logo_filename, :string
  end

  def self.down
  end
end
