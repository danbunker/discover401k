class FixClientDocumentsThree < ActiveRecord::Migration
  def self.up
  	rename_column(:client_documents, :company_id, :company_client_id)
  end

  def self.down
  end
end
