class AddTrialColumn < ActiveRecord::Migration
  def self.up
  	add_column :companies, :free_trial_flag, :boolean
  end

  def self.down
  end
end
