class AddHiddenClassCol < ActiveRecord::Migration
  def self.up
  	add_column :companies_planning_classes, :hidden_flag, :boolean, :default => false
  end

  def self.down
  end
end
