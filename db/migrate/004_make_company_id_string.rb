class MakeCompanyIdString < ActiveRecord::Migration
  def self.up
  	change_column(:companies, :identifier, :string)
  end

  def self.down
  end
end
