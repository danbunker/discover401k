class AddBlobToPdf < ActiveRecord::Migration
  def self.up
  	add_column :pdf_docs, :pdf, :binary, :limit => 5.megabytes
  end

  def self.down
  end
end
