class CreateCompanyClients < ActiveRecord::Migration
  def self.up
    create_table :company_clients do |t|
    	t.column :company_id, :integer
    	t.column :client_id, :string
    end
  end

  def self.down
    drop_table :company_clients
  end
end
